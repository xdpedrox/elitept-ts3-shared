import { ChannelEdit } from "ts3-nodejs-library/src/types/PropertyTypes"

export const privateChannelProperties: ChannelEdit = {
  channelFlagPermanent: true,
  channelCodecQuality: 10,
  channelMaxclients: 0,
  channelMaxfamilyclients: 0,
  channelFlagMaxclientsUnlimited: false,
}

export const publicChannelProperties: ChannelEdit = {
  channelFlagPermanent: true,
  channelCodecQuality: 10,
  channelMaxclients: -1,
  channelMaxfamilyclients: -1,
  channelFlagMaxclientsUnlimited: true,
}
