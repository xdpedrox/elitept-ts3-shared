export const groupSettings = {
  channelOwner: "13",
  channelAdmin: "13",
  channelMod: "14",
  channelMember: "15",
  guestChannelGroup: "8",
  serverGroupTemplate: "7",
}

export const ChannelGroupsAdmin = [
  groupSettings.channelAdmin,
  groupSettings.channelMod,
  groupSettings.channelMember,
]

export const createChannelServerGroups = [10]

export const channelSettings = {
  defaultChannelCid: "1",
  startChannel: "2",
  endChannel: "3",
  freeSpacerName: "Free Channel",
  freeChannelName: "♦ Vaga ♦",
  subChannelName: "● Sala de Convivio",
  awayChannelName: "● AFK/Away",
  spacerBar: "━",
}

export const notCrawl = [2]
