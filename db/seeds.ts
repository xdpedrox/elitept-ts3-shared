import db from "./index"

/*
 * This seed function is executed when you run `blitz db seed`.
 *
 * Probably you want to use a library like https://chancejs.com
 * to easily generate realistic data.
 */
const seed = async () => {
  // ServerGroups
  await db.teamSpeakServerGroup.create({
    data: {
      name: "Minecraft",
      sgid: 14,
      type: "FUN",
      fileUrl: "https://www.prock.dev/images/icons/apollo.svg",
    },
  })
  await db.teamSpeakServerGroup.create({
    data: {
      name: "CS2",
      sgid: 15,
      type: "FUN",
      fileUrl: "https://www.prock.dev/images/icons/apollo.svg",
    },
  })
  await db.teamSpeakServerGroup.create({
    data: {
      name: "Metin2",
      sgid: 16,
      type: "FUN",
      fileUrl: "https://www.prock.dev/images/icons/apollo.svg",
    },
  })
  await db.teamSpeakServerGroup.create({
    data: {
      name: "Apex",
      sgid: 17,
      type: "FUN",
      fileUrl: "https://www.prock.dev/images/icons/apollo.svg",
    },
  })
  await db.teamSpeakServerGroup.create({
    data: {
      name: "COD",
      sgid: 18,
      type: "FUN",
      fileUrl: "https://www.prock.dev/images/icons/apollo.svg",
    },
  })
}

export default seed
